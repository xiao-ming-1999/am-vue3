import { createApp } from 'vue'

import App from './App.vue'
import { router } from './router/index';
import { createPinia } from 'pinia'

import * as ElementPlusIconsVue from '@element-plus/icons-vue'



const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.use(router)


// 样式引入
import 'element-plus/dist/index.css' // el样式
import 'virtual:windi.css' // windi css 样式
import 'nprogress/nprogress.css' // nprogress(全屏loading)
import './assets/main.css'

import "./permission.js";

// 自定义指注册
import permission from "@/directives/permission.js";
app.use(permission)
// 全局注册element-plus icon图标组件
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

app.mount('#app')
