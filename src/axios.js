import axios from "axios";
import { getToken } from "@/composables/auth.js";
import { toast } from "@/composables/util.js";
import { mainStore } from '@/store/index.js'

const service = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API
})


// 请求拦截器
service.interceptors.request.use(function (config) {
  // 设置请求头
  const token = getToken()
  if (token) {
    config.headers['token'] = token
  }

  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);

});

// 响应拦截器
service.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  // 这里是对文件类型做判断，文件类型只有一层data
  return response.request.responseType == 'blob' ? response.data : response.data.data;
}, function (error) {
  // 对响应错误做点什么
  const store = mainStore()
  const msg = error.response.data.msg || '请求失败'
  if (msg === '非法token，请先登录！') {
    store.LOGOUT().finally(() => location.reload())
  }
  toast(msg, 'error')

  return Promise.reject(error);
});

export default service
