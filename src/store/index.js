// 1、定义状态容器
// 2、修改容器中的state
// 3、仓库中的action的使用

import { defineStore } from "pinia";
import { getInfo } from '@/api/manager.js'
import { removeToken } from '@/composables/auth.js'

// defineStore参数1为仓库id(唯一值)
export const mainStore = defineStore('main', {
  state: () => {
    return {
      // 用户信息
      user: {},
      // 侧边栏宽度
      asideWidth: '250px',
      // 侧边菜单
      menus: [],
      // 用户权限
      ruleNames: []
    }
  },
  getters: {
  },
  actions: {
    SET_USERINFO (userInfo) {
      this.user = userInfo
    },
    // 获取用户信息 并且 设置用户信息
    GET_INFO () {
      return new Promise((resolve, reject) => {
        getInfo().then(res => {
          // 存储用户信息
          this.SET_USERINFO(res)
          // 存储用户菜单
          this.menus = res.menus
          // 存储用户权限
          this.ruleNames = res.ruleNames
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    async LOGOUT () {
      // 清除cookie内的token
      removeToken()
      // 清除当前用户状态 vuex
      this.SET_USERINFO({})
    },
    HANDLEASIDEWIDTH () {
      this.asideWidth = this.asideWidth === '250px' ? '60px' : '250px'
    }
  }
})
