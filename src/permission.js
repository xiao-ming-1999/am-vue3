import { router } from '@/router/index.js'
import { getToken } from "@/composables/auth.js";
import { toast, showFullLoading, hideFullLoading } from "@/composables/util.js";
import { mainStore } from "@/store/index.js";
import { addRoutes } from '@/router/index.js'

// 防止多次调用getInfo接口，声明在外面，就不会每次触发前置守卫就更新值了
let hasGetInfo = false

// 全局前置守卫
router.beforeEach(async (to, from, next) => {
  const store = mainStore()
  const token = getToken()
  const title = `${to.meta.title || ''}-vue3商城后台管理`

  showFullLoading()

  // 没有登录，强制跳回登录
  if (!token && to.path !== '/login') {
    toast('请先登录~默认账号密码：admin', 'error')
    return next({ path: "/login" })
  }
  // 防止重复登录检验
  if (token && to.path === '/login') {
    toast('请勿重复登录', 'error')
    return next({ path: from.path || '/' })
  }
  // 检测是否有新的路由，用于解决刷新404问题，路由需要手动指向路径 next(to.fullPath)
  let hasNewRoutes = false

  if (token && !hasGetInfo) {
    const { menus } = await store.GET_INFO()
    hasGetInfo = true
    hasNewRoutes = addRoutes(menus)
  }

  document.title = title

  hasNewRoutes ? next(to.fullPath) : next()

})

// 全局后置守卫
router.afterEach((to, from) => {
  hideFullLoading()
})
