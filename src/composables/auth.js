import { useCookies } from '@vueuse/integrations/useCookies'
const cookie = useCookies()
const tokenKey ='admin-token'

export function getToken(){
 return cookie.get('admin-token')
}

export function setToken(token){
  cookie.set(tokenKey, token)
}

export function removeToken(){
  cookie.remove(tokenKey)
}
