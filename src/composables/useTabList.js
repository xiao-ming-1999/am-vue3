import { ref } from 'vue'
import { useRouter, useRoute, onBeforeRouteUpdate } from 'vue-router'
import { mainStore } from '@/store/index'
import { useCookies } from '@vueuse/integrations/useCookies'
export function useTabList (params) {
  const store = mainStore()
  const route = useRoute()
  const router = useRouter()
  const cookie = useCookies()

  const activeTab = ref(route.path)
  const tabList = ref([
    {
      path: '/',
      title: '后台首页'
    }
  ])
  // 初始化tabList
  function initTabList () {
    const tabs = cookie.get('tabList')
    if (tabs) {
      tabList.value = tabs
    }
  }
  initTabList()
  // 监听事件， activeTab 变化触发 实参为activeTab变化后的值
  function changeTab (path) {
    router.push(path)
  }
  // 下拉框 关闭tab事件
  function handleClose (e) {
    // 关闭其他
    if (e === 'clearOther') {
      tabList.value = tabList.value.filter(
        (item) => item.path === '/' || item.path === activeTab.value
      )
    }
    if (e === 'clearAll') {
      tabList.value = tabList.value.filter((item) => item.path === '/')
      activeTab.value = '/'
    }
    cookie.set('tabList', tabList.value)
  }

  // 删除逻辑：判断是否为高亮tab，如果是，则给高亮tab重新赋值（赋值规则：高亮的上一个或高亮的下一个）
  function removeTab (t) {
    // 声明变量赋值是为了简化代码.value写法会使代码冗余
    let tabs = tabList.value
    let a = activeTab.value

    if (a === t) {
      tabs.forEach((item, index) => {
        if (item.path === t) {
          const nextTab = tabs[index + 1] || tabs[index - 1]
          if (nextTab) {
            a = nextTab.path
          }
        }
      })
    }
    activeTab.value = a
    tabList.value = tabList.value.filter((item) => item.path !== t)
    cookie.set('tabList', tabList.value)
  }
  // tabList添加事件（路由改变添加tbaList）
  function addTabs (tab) {
    const noTab = tabList.value.findIndex((o) => o.path === tab.path) === -1
    if (noTab) {
      tabList.value.push(tab)
    }
    cookie.set('tabList', tabList.value)
  }
  // 监听路由更新事件
  onBeforeRouteUpdate((to, from) => {
    activeTab.value = to.path
    // 获取路由信息
    addTabs({ path: to.path, title: to.meta.title })
  })
  return {
    store,
    activeTab,
    tabList,
    changeTab,
    removeTab,
    handleClose
  }
}
