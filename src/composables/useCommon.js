import { ref, reactive, computed } from 'vue'
import { toast } from '@/composables/util.js'
// 页面公共部分（分页+列表+删除+搜索，修改状态）逻辑拆分
// opt参数 必传：getList(获取列表数据的接口) 
// 选传：searchForm(搜索参数)、updateStatus(修改状态的接口)、
// delete(删除状态的接口)、onGetListSuccess(获取数据后对数据进行处理的回调)
export function useInitTable (opt = {}) {
  const tableData = ref([])
  const loading = ref(false)
  // 分页参数
  const currentPage = ref(1)
  const limit = ref(10)
  const total = ref(0)
  // 搜索
  let searchForm = null
  let resetSearchForm = null
  // 搜索参数可能会有多个，需要使用组件传递对应搜索参数，公共组件动态获取
  if (opt.searchForm) {
    searchForm = reactive({ ...opt.searchForm })

    resetSearchForm = () => {
      // opt.searchForm的格式searchForm: {keyword: ''}，使用组件传的值必定为空，循环给searchForm初始化值
      for (const key in opt.searchForm) {
        searchForm[key] = opt.searchForm[key]
      }
      getData()
    }
  }

  const getData = async (p = null) => {
    // p为当前页码数
    if (typeof p == 'number') {
      currentPage.value = p
    }
    loading.value = true
    const res = await opt.getList(currentPage.value, searchForm)
    // 部分组件需要返回特殊的参数，如：每个item中都要返回一个checked属性，那么将执行使用组件传来的逻辑，返回对应参数
    if (opt.onGetListSuccess && typeof opt.onGetListSuccess == 'function') {
      opt.onGetListSuccess(res)
    } else {
      tableData.value = res.list
      total.value = res.totalCount
    }
    loading.value = false
  }
  getData()
  // 修改状态
  const handleStatusChange = async (status, row) => {
    row.statusLoading = true
    await opt.updateStatus(row.id, status)
    row.statusLoading = false
    toast('修改状态成功')
    row.status = status
    getData()
  }
  // 删除
  const handleDelete = async (id) => {
    loading.value = true
    try {
      await opt.delete(id)
      toast('删除成功')
      getData()
    } catch (err) {
      loading.value = false
    }
  }

  // 复选框多选选中id
  const multiSelectionIds = ref([])
  const handleSelectionChange = (e) => {
    const ids = e.map(o => { return o.id })
    multiSelectionIds.value = ids
  }
  // 批量删除
  const multipleTableRef = ref(null)
  const handleMultiDelete = async () => {
    if (!multiSelectionIds.value.length) return toast('请选择至少一个选项', 'warning')
    try {
      await opt.delete(multiSelectionIds.value)
      toast('删除成功')
      getData()
      multipleTableRef.value.clearSelection()
    } catch (err) {
      console.log(err, 'err');
    }
  }
  // 批量修改状态
  const handleMultiStatusChange = async (status) => {
    if (!multiSelectionIds.value.length) return toast('请选择至少一个选项', 'warning')
    try {
      await opt.updateStatus(multiSelectionIds.value,status)
      toast('修改状态成功')
      getData()
      multipleTableRef.value.clearSelection()
    } catch (err) {
      console.log(err, 'err');
    }
  }

  return {
    searchForm,
    resetSearchForm,
    tableData,
    limit,
    loading,
    total,
    currentPage,
    getData,
    handleStatusChange,
    handleDelete,
    handleSelectionChange,
    multipleTableRef,
    handleMultiDelete,
    handleMultiStatusChange,
    multiSelectionIds
  }
}

// 表单（新增+修改+提交）逻辑拆分
// opt参数 必传：form(表单初始值)、
// 可选：title(表单标题)、currentPage(当前页：必须在useInitTable之后)、
// update(修改表单接口)、create(新增表单接口)
export function useInitForm (opt = {}) {

  const formDrawerRef = ref(null)
  const formRef = ref(null)
  // 表单参数
  const defaultForm = opt.form
  let form = reactive({})
  // 表单规则
  const rules = opt.rules || {}
  // 修改id
  const editId = ref(0)
  // 弹框title
  const drawerTitle = computed(() => {
    return editId.value ? '修改' + opt.title : '新增' + opt.title
  })
  // 当前页码数
  const currentPage = ref(1)
  currentPage.value = opt.currentPage

  // 提交表单
  const handleSubmit = () => {
    formRef.value.validate(async (valid) => {
      if (!valid) return false
      formDrawerRef.value.showLoading()
      try {
        // 将form赋值给body，传给回调，处理时间戳
        let body ={}
        if(opt.beforeSubmit && typeof opt.beforeSubmit =='function' ) {
          body =opt.beforeSubmit({...form})
        }else {
          body = form
        }

        const Fun = editId.value
          ? opt.update(editId.value, body)
          : opt.create(body)
        const data = await Fun

        toast(drawerTitle.value + '成功')
        // 修改刷新当前页，新增刷新第一页
        opt.getData(editId.value ? currentPage.value : 1)
        formDrawerRef.value.close()
      } catch (err) {
        console.log(err);
      }
      formDrawerRef.value.hideLoading()
    })
  }
  // 重置表单
  const resetForm = (row = {}) => {
    if (formRef.value) formRef.value.clearValidate()
    // 这里for in defaultForm的原因是，defaultForm为原始值。
    // 即：原始值为{title:'xx',content:'xx'}
    // 触发编辑事件后 form的值就会为row的值 
    // 即：{title:'xx',content:'xx',create_time:'2022-12',update_time:'2022-12'}
    // 当点击修改后，如果for in的是参数row的话，form的原始数据结构会被改变，会向接口传多余参数，导致报错
    // 所以for in defaultForm就是为了点击修改时，给form的key规定好为原始的key值
    for (const key in defaultForm) {
      form[key] = row[key]
    }
  }
  // 新增
  const handleCreate = () => {
    editId.value = 0
    resetForm(defaultForm)
    formDrawerRef.value.open()
  }
  // 编辑
  const handleEdit = (row) => {
    editId.value = row.id
    resetForm(row)
    formDrawerRef.value.open()
  }

  return {
    formDrawerRef,
    formRef,
    form,
    rules,
    editId,
    drawerTitle,
    handleSubmit,
    resetForm,
    handleCreate,
    handleEdit
  }

}


