import nprogress from "nprogress";
// 轻提示
export function toast (message, type = 'success', dangerouslyUseHTMLString = true) {
  ElNotification({
    message,
    type,
    dangerouslyUseHTMLString,
    duration: 3000
  })
}
// 提示消息
export function showModel (content = '提示内容', type = 'success', title = '') {
  return ElMessageBox.confirm(
    content,
    title,
    {
      confirmButtonText: '确认',
      cancelButtonText: '取消',
      type,
    }
  )
}
// 消息弹出框
export function showPrompt (tip, value = '') {
  return ElMessageBox.prompt('', tip, {
    confirmButtonText: '确认',
    cancelButtonText: '取消',
    inputValue: value
  })
}
// 显示全屏loading
export function showFullLoading () {
  nprogress.start()
}

// 隐藏全屏loading
export function hideFullLoading () {
  nprogress.done()
}

// 将query对象转成url参数
export function queryParams (query) {
  let q = []
  for (const key in query) {
    if (query[key]) {
      // encodeURIComponent() 函数可把字符串作为 URI 组件进行编码。
      // 例：var uri="https://www.runoob.com/my test.php?name=ståle&car=saab";
      // document.write(encodeURIComponent(uri));
      // 输出结果：https%3A%2F%2Fwww.runoob.com%2Fmy%20test.php%3Fname%3Dst%C3%A5le%26car%3Dsaab
      q.push(`${key}=${encodeURIComponent(query[key])}`)
    }
  }
  let r = q.join("&") // limit=10&keyword=ceshi
  r = r ? "?" + r : ''
  return r
}

// 上移
export function useArrayMoveUp(arr,index) {
  swapArray(arr,index,index-1)
}
// 下移
export function useArrayMoveDown(arr,index) {
  swapArray(arr,index,index+1)
}
// 排序方法 (排序数组，当前索引，和需要换位的索引)
function swapArray(arr,index1,index2) {
  arr[index1]=arr.splice(index2,1,arr[index1])[0]
  return arr
}

// sku排列算法
export function cartesianProductOf() {
  return Array.prototype.reduce.call(arguments, function (a, b) {
      var ret = [];
      a.forEach(function (a) {
          b.forEach(function (b) {
              ret.push(a.concat([b]));
          });
      });
      return ret;
  }, [
      []
  ]);
}
