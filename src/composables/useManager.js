// 代码封装
// Fheader.vue
import { ref, reactive } from 'vue'
import { logout, updatePassword } from '@/api/manager.js'
import { mainStore } from '@/store/index.js'
// import { storeToRefs } from "pinia";
import { showModel, toast } from '@/composables/util.js'
import { useRouter,useRoute } from 'vue-router'
// Fheader.vue 代码封装
export function useRePassword () {
  const router = useRouter()
  const store = mainStore()
  // 表单值
  let form = reactive({ oldpassword: '', password: '', repassword: '' })
  // 表单检验规则
  const rules = reactive({
    oldpassword: [
      { required: true, message: '旧密码不能为空', trigger: 'blur' }
    ],
    password: [{ required: true, message: '新密码不能为空', trigger: 'blur' }],
    repassword: [
      { required: true, message: '确认密码不能为空', trigger: 'blur' }
    ]
  })
  // form表单Ref
  const formRef = ref(null)
  // 抽屉Ref
  const formDrawerRef = ref(null)
  // 确认按钮事件
  const onSubmit = () => {
    formRef.value.validate((valid) => {
      if (!valid) return false

      formDrawerRef.value.showLoading()
      updatePassword(form)
        .then((res) => {
          toast('修改密码成功，请重新登录')
          store.dispatch('logout')
          // // 跳转回登录页
          router.push('/login')
        })
        .finally(() => {
          formDrawerRef.value.hideLoading()
        })
    })
  }
  // 打开抽屉
  const openRePasswordForm = function () {
    formDrawerRef.value.open()
  }
  return {
    form,
    rules,
    formRef,
    formDrawerRef,
    onSubmit,
    openRePasswordForm
  }
}
// 退出登录
export function useLogout () {
  const router = useRouter()
  const route =useRoute()
  const store = mainStore()
  function handleLogout () {
    showModel('是否退出', 'warning')
      .then(async (res) => {
        await logout()
        store.LOGOUT()
        // 4、提示退出登录成功
        toast('退出登录成功')
        // 5、跳回登录页 并携带当前页面路径
        router.push('/login?return_url='+encodeURIComponent(route.fullPath))
      })
      .catch((err) => {
        console.log(err, 'err')
      })
  }
  return {
    handleLogout
  }
}
