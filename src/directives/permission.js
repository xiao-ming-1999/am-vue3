import { mainStore } from '@/store/index.js'
function hasPermission (value, el = false) {

  if (!Array.isArray(value)) {
    throw new Error('需要配置权限，例如 v-permission="["getStatistics2","GET"]"')
  }
  const store = mainStore()
  // value为数组
  // includes 方法可以判断一个数组中是否包含某一个元素, 并且返回true  或者false
  // findIndex：获取第一个符合判断条件的值索引，若返回值为布尔，true为0，false为-1
  const hasAuth = value.findIndex(v => store.ruleNames.includes(v)) != -1
  // hasAuth为false则说明没有权限
  // 如果有元素且没有权限，则获取该元素父节点，删除其子节点
  if(el && !hasAuth) {
    el.parentNode && el.parentNode.removeChild(el) 
  }
  return hasAuth
}
export default {
  install (app) {
    // 自定义指令 permission
    app.directive('permission', {
      mounted (el, binding) { // el元素节点 binding.value：v-permission绑定的值
        hasPermission(binding.value, el)
      }
    })
  }
}
