// 图库管理
import axios from "@/axios";

// 指定分类下的图片列表
export function getImageList (id, page = 1) {
  return axios.get(`/admin/image_class/${id}/image/${page}?limit=12`)
}
// 修改图片名称
export function updateImage (id, name) {
  return axios.post(`/admin/image/${id}`, {
    name
  })
}
// 删除图片
export function deleteImage (ids) {
  return axios.post('/admin/image/delete_all', {
    ids
  })
}

// 图片上传地址 /api会通过proxy转换为 http://ceshi13.dishait.cn
export const uploadImageBaseUrl = import.meta.env.VITE_APP_BASE_API + "/admin/image/upload"
