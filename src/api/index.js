// 后台统计(首页接口)
import axios from "@/axios";

export function getStatistics1 (username, password) {
  return axios.get('/admin/statistics1')
}

export function getStatistics2 () {
  return axios.get('/admin/statistics2')
}


export function getStatistics3 (type) {
  return axios.get('/admin/statistics3?type=' + type)
}


