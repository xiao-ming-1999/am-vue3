// 图库分类
import axios from "@/axios";

// 图库列表
export function getImageClassList (page) {
  return axios.get('/admin/image_class/' + page)
}
// 新增图库分类
export function createImageClassList (data) {
  return axios.post('/admin/image_class', data)
}
// 修改图库分类
export function updateImageClassList (id, data) {
  return axios.post(`/admin/image_class/${id}`, data)
}
// 删除图库分类
export function deleteImageClassList (id) {
  return axios.post(`/admin/image_class/${id}/delete`)
}
