// 菜单权限管理
import axios from "@/axios";

// 菜单权限列表
export function getRuleList (page) {
  return axios.get(`/admin/rule/${page}`)
}
// 增加菜单权限
export function createRule (data) {
  return axios.post(`/admin/rule`, data)
}
// 修改菜单权限
export function updateRule (id, data) {
  return axios.post(`/admin/rule/${id}`, data)
}
// 删除菜单权限
export function deleteRule (id) {
  return axios.post(`/admin/rule/${id}/delete`)
}
// 修改菜单权限状态(是否启用)
export function updateRuleStatus (id, status) {
  return axios.post(`/admin/rule/${id}/update_status`, { status })
}
