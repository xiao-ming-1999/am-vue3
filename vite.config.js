import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import WindiCSS from 'vite-plugin-windicss'
import Icons from 'unplugin-icons/vite'
// import IconsResolver from 'unplugin-icons/resolver'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'

import path from "path";

const pathSrc = path.resolve(__dirname, 'src')

// https://vitejs.dev/config/
export default defineConfig({
  resolve: {
    alias: {
      // 配置src别名
      '@': pathSrc
    },
  },
  // 跨域代理
  server: {
    proxy: {
      // 选项写法
      '/api': {
        // 香港后端
        target: 'http://ceshi13.dishait.cn/',
        // 内地后端
        // target: 'http://sceshi14.dishait.cn',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      },
    }
  },
  css: {
    preprocessorOptions: {
      scss: true
    }
  },
  plugins: [
    vue(),
    WindiCSS(),
    AutoImport({
      // 自动导入 Vue 相关函数，如：ref, reactive, toRef 等
      // imports: ['vue'],

      resolvers: [
        // 自动导入 Element Plus 组件
        ElementPlusResolver(),

        // 自动导入图标组件
        // 自动引入图标废除，主页菜单会涉及到 动态渲染菜单图标，自动导入无法根据动态值来正确引入图标（以改为全局引入图标）
        // IconsResolver(
        //   {
        //     prefix: 'Icon',
        //   }
        // ),
      ],

      dts: path.resolve(pathSrc, 'auto-imports.d.ts'),
    }),

    Components({
      resolvers: [
        // 自动注册图标组件
        // IconsResolver(
        //   {
        //     enabledCollections: ['ep'],
        //   }
        // ),
        // 自动导入 Element Plus 组件
        ElementPlusResolver(),
      ],

      dts: path.resolve(pathSrc, 'components.d.ts'),
    }),

    Icons({
      autoInstall: true,
    }),
  ],

})
